import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Login from "./components/login/Login";
import PrivateRoute from "./PrivateRoute";
import Dashboard from "./components/dashboard/Dashboard";
import { Provider } from "react-redux";
import store from "./redux/store";
import { NotificationContainer } from "react-notifications";
import "react-notifications/dist/react-notifications.css";

function App() {
  return (
    <Provider store={store}>
      <NotificationContainer />
      <Router>
        <Switch>
          <Route path='/login' exact component={Login} />
          <PrivateRoute path='/' exact component={Dashboard} />
        </Switch>
      </Router>
    </Provider>
  );
}

export default App;
