import React from "react";
import PropTypes from "prop-types";
import Navbar from "../navbar/Navbar";
import { HashRouter as Router, Route, Switch } from "react-router-dom";
import Users from "../pages/users/Users";
import Posts from "../pages/posts/Posts";

const Dashboard = (props) => {
  return (
    <Router>
      <div>
        <Navbar />
      </div>

      <div>
        <Switch>
          <Route path='/users' component={Users} exact />
          <Route path='/posts' component={Posts} exact />
        </Switch>
      </div>
    </Router>
  );
};

Dashboard.propTypes = {};

export default Dashboard;
