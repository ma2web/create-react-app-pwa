import React from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import { logout } from "../../redux/actions/auth";
import { connect } from "react-redux";

const Navbar = ({ logout }) => {
  return (
    <div>
      <ul>
        <li>
          <Link to='/users'>users</Link>
        </li>
        <li>
          <Link to='/posts'>posts</Link>
        </li>
        <li>
          <a onClick={logout} href='/login'>
            <i className='fas fa-sign-out-alt'></i>{" "}
            <span className='hide-sm'>Logout</span>
          </a>
        </li>
      </ul>
    </div>
  );
};

Navbar.propTypes = {
  logout: PropTypes.func.isRequired,
};

export default connect(null, { logout })(Navbar);
